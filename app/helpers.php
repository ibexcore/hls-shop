<?php

function dump()
{
    foreach (func_get_args() as $arg) {
        var_dump($arg);
    }
}

function dd()
{
    call_user_func_array('dump', func_get_args());
    die();
}
