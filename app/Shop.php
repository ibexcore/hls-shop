<?php

namespace App;

use DateTime;
use DateInterval;
use Carbon\Carbon;
use App\Exceptions\InvalidDateException;

class Shop
{

    /**
     * Is the shop open on the provided date/time
     * If provided a DateTime object, check relative to that, otherwise use now
     *
     * @param DateTime $dt
     *
     * @return boolean
     * @throws \App\Exceptions\InvalidDateException
     */
    public function isOpen(DateTime $dt = null)
    {
        if (!$dt) {
            $dt = new DateTime;
        }
        $this->validateDay($dt);

        // First, we check if the provided date is a holiday date.
        if (in_array($dt->format('Y-m-d'), $this->getHolidayDays())) {
            return false;
        }

        $hours = $this->parseHours($dt);

        // now check if it is open!
        foreach ($hours as $open => $close) {
            $firstTime = Carbon::parse($open);
            // We minus one second so a query like is open at 17:00:00 will return false but a query like
            // 16:59:59 will return true
            $secondTime = Carbon::parse($close)->subSecond();

            if (Carbon::parse($dt->format('Y-m-d H:i:s'))->between($firstTime, $secondTime)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Is the shop closed on the provided date/time
     * If provided a DateTime object, check relative to that, otherwise use now
     *
     * @param DateTime $dt
     *
     * @return boolean
     * @throws \App\Exceptions\InvalidDateException
     */
    public function isClosed(DateTime $dt = null)
    {
        return !($this->isOpen($dt));
    }


    /**
     * At what date/time will the shop next be open
     * If provided a DateTime object, check relative to that, otherwise use now
     * If the shop is already open, return the provided datetime/now
     *
     * @param DateTime $dt
     *
     * @return DateTime
     * @throws \App\Exceptions\InvalidDateException
     * @throws \Exception
     */
    public function nextOpen(DateTime $dt = null)
    {
        if (!$dt) {
            $dt = new DateTime;
        }

        $holidays = $this->getHolidayDays();
        if (in_array($dt->format('Y-m-d'), $holidays)) {
            $dt = $dt->add(new DateInterval('P1D'));
            return $this->nextOpen($dt);
        }

        $this->validateDay($dt);

        $hours = $this->parseHours($dt);

        foreach ($hours as $open => $close) {
            $firstTime = Carbon::parse($open);

            $date = Carbon::parse($dt->format('Y-m-d H:i:s'));
            if ($date->lessThanOrEqualTo($firstTime)) {
                return $firstTime;
            }
        }

        // move onto the next day...
        $dt = $dt->add(new DateInterval('P1D'))->format('Y-m-d');
        $dt = DateTime::createFromFormat('Y-m-d H:i:s', $dt . ' 08:00:00');
        return $this->nextOpen($dt);
    }


    /**
     * At what date/time will the shop next be closed
     * If provided a DateTime object, check relative to that, otherwise use now
     * If the shop is already closed, return the provided datetime/now
     *
     * @param DateTime $dt
     *
     * @return DateTime
     * @throws \App\Exceptions\InvalidDateException
     * @throws \Exception
     */
    public function nextClosed(DateTime $dt = null)
    {
        if (!$dt) {
            $dt = new DateTime;
        }

        $holidays = $this->getHolidayDays();
        if (in_array($dt->format('Y-m-d'), $holidays)) {
            $dt = $dt->add(new DateInterval('P1D'));
            return $this->nextClosed($dt);
        }

        $this->validateDay($dt);

        $hours = $this->parseHours($dt);

        foreach ($hours as $open => $close) {
            $firstTime = Carbon::parse($close);

            $date = Carbon::parse($dt->format('Y-m-d H:i:s'));
            if ($date->lessThanOrEqualTo($firstTime)) {
                return $firstTime;
            }
        }

        // move onto the next day...
        $dt = $dt->add(new DateInterval('P1D'))->format('Y-m-d');
        $dt = DateTime::createFromFormat('Y-m-d H:i:s', $dt . ' 08:00:00');
        return $this->nextClosed($dt);
    }

    /**
     * @param \DateTime $dt
     * @return mixed|string
     * @throws \App\Exceptions\InvalidDateException
     */
    public function getOpeningTimes(DateTime $dt)
    {
        $holidayDays = $this->getHolidayDays();
        if (in_array($dt->format('Y-m-d'), $holidayDays)) {
            return 'Closed - bank holiday';
        }

        $day = ucfirst($dt->format('l'));
        $times = $this->loadTimesConfig();
        if (!isset($times[$day])) {
            throw new InvalidDateException;
        }
        return $times[$day];
    }

    /**
     * Get all of the holiday dates as a flat array
     *
     * @return array
     */
    public function getHolidayDays(): array
    {
        // Get all of the dates between the holidays defined in the config.
        $dates = array_map(function ($value) {
            $start = Carbon::parse($value['start']);
            $end = Carbon::parse($value['end']);
            $dates = [];
            for ($date = $start; $date->lte($end); $date->addDay()) {
                $dates[] = $date->format('Y-m-d');
            }
            return $dates;
        }, $this->loadHolidaysConfig());
        // Flatten the multi-dimensional array into a "flat" array
        return array_reduce($dates, 'array_merge', []);
    }

    /**
     * @param \DateTime $dateTime
     * @return array
     */
    protected function parseHours(DateTime $dateTime)
    {
        $times = $this->loadTimesConfig();
        $day = $dateTime->format('l');
        $hours = [];
        // While this array chunk is fine for our grouping, if the keys in the config were ever to swap or be unordered,
        // we'd have to find another way of doing it.
        foreach (array_chunk($times[$day], 2, true) as $item) {
            $flipped = array_flip($item);
            $hours[date($dateTime->format('Y-m-d ' . $flipped['open']))] = date($dateTime->format('Y-m-d ' . $flipped['closed']));
        }
        return $hours;
    }

    /**
     * @param \DateTime $dt
     * @return string
     * @throws \App\Exceptions\InvalidDateException
     */
    protected function validateDay(DateTime $dt)
    {
        $day = ucfirst($dt->format('l'));
        $times = $this->loadTimesConfig();
        if (!isset($times[$day])) {
            throw new InvalidDateException;
        }
        return $day;
    }

    protected function loadHolidaysConfig(): array
    {
        return $this->loadConfig('holidays');
    }

    /**
     * @return array
     */
    protected function loadTimesConfig(): array
    {
        return $this->loadConfig('times');
    }

    /**
     * Load a config
     *
     * @param string $name
     * @return array
     */
    protected function loadConfig(string $name)
    {
        return include dirname(__DIR__) . "/config/{$name}.config.php";
    }
}
