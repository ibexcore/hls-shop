<?php
error_reporting(-1);
ini_set("display_errors", 1);
include dirname(__DIR__) . '/vendor/autoload.php';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">

    <title>Shop</title>
    <link href="//stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>
Opening times <?php echo date('l'); ?>:<br>
<?php
$shop = new \App\Shop();
$date = new DateTime;
$times = array_keys($shop->getOpeningTimes($date));
/*$last = null;
foreach (array_chunk($shop->getOpeningTimes($date), 2, true) as $value) {
    $value = array_flip($value);
    echo "Open: {$value['open']} | Close: {$value['closed']}<br>";
}*/
if (count($times) == 4) {
    echo "Open: {$times[0]} | Closed for lunch from: {$times[1]} until {$times[2]} | Closed until tomorrow from: {$times[3]}";
} else {
    echo $times; //closed all day
}
?>

<hr>
Holiday days this year:<br>

<?php
foreach ($shop->getHolidayDays() as $date) {
    echo "{$date}<br>";
}
?>
</body>
</html>
