<?php

use PHPUnit\Framework\TestCase;

class ShopTest extends TestCase
{
    /**
     * @var \App\Shop
     */
    protected $shop;

    public function setUp()
    {
        $this->shop = new App\Shop();
    }

    public function test_is_open()
    {
        $this->assertTrue($this->shop->isOpen(new DateTime('2018-05-04 09:00:00')));
        $this->assertFalse($this->shop->isOpen(new DateTime('2018-05-04 08:59:59')));
        $this->assertTrue($this->shop->isOpen(new DateTime('2018-05-04 09:01:00')));
        $this->assertTrue($this->shop->isOpen(new DateTime('2018-05-04 12:29:59')));
        $this->assertFalse($this->shop->isOpen(new DateTime('2018-05-04 12:30:00')));
        $this->assertFalse($this->shop->isOpen(new DateTime('2018-05-04 13:29:59')));
        $this->assertTrue($this->shop->isOpen(new DateTime('2018-05-04 13:30:00')));
        $this->assertTrue($this->shop->isOpen(new DateTime('2018-05-04 16:59:59')));
        $this->assertFalse($this->shop->isOpen(new DateTime('2018-05-04 17:00:00')));

        // bank holidays
        $this->assertFalse($this->shop->isOpen(new DateTime('2017-01-01 11:00:00')));
        $this->assertFalse($this->shop->isOpen(new DateTime('2017-12-21 10:00:00')));
    }

    public function test_is_closed()
    {
        $this->assertFalse($this->shop->isClosed(new DateTime('2018-05-04 09:00:00')));
        $this->assertTrue($this->shop->isClosed(new DateTime('2018-05-04 08:59:59')));
        $this->assertFalse($this->shop->isClosed(new DateTime('2018-05-04 09:01:00')));
        $this->assertFalse($this->shop->isClosed(new DateTime('2018-05-04 12:29:59')));
        $this->assertTrue($this->shop->isClosed(new DateTime('2018-05-04 12:30:00')));
        $this->assertTrue($this->shop->isClosed(new DateTime('2018-05-04 13:29:59')));
        $this->assertFalse($this->shop->isClosed(new DateTime('2018-05-04 13:30:00')));
        $this->assertFalse($this->shop->isClosed(new DateTime('2018-05-04 16:59:59')));
        $this->assertTrue($this->shop->isClosed(new DateTime('2018-05-04 17:00:00')));
        $this->assertTrue($this->shop->isClosed(new DateTime('01-01-2017 11:00:00')));

        // bank holidays
        $this->assertFalse($this->shop->isOpen(new DateTime('2017-01-01 11:00:00')));
        $this->assertFalse($this->shop->isOpen(new DateTime('2017-12-21 10:00:00')));
    }

    public function test_next_open()
    {
        $next = $this->shop->nextOpen(new DateTime('2018-05-04 08:55:55'));
        $this->assertEquals('2018-05-04 09:00:00', $next->format('Y-m-d H:i:s'));

        $next = $this->shop->nextOpen(new DateTime('2018-05-04 10:00:00'));
        $this->assertEquals('2018-05-04 13:30:00', $next->format('Y-m-d H:i:s'));

        $next = $this->shop->nextOpen(new DateTime('2018-05-04 17:30:00'));
        $this->assertEquals('2018-05-07 09:00:00', $next->format('Y-m-d H:i:s'));

        $next = $this->shop->nextOpen(new DateTime('2017-01-01 08:55:55'));
        $this->assertEquals('2017-01-02 09:00:00', $next->format('Y-m-d H:i:s'));
    }

    public function test_next_closed()
    {
        $next = $this->shop->nextClosed(new DateTime('2018-05-04 08:55:55'));
        $this->assertEquals('2018-05-04 12:30:00', $next->format('Y-m-d H:i:s'));

        $next = $this->shop->nextClosed(new DateTime('2018-05-04 09:55:55'));
        $this->assertEquals('2018-05-04 12:30:00', $next->format('Y-m-d H:i:s'));

        $next = $this->shop->nextClosed(new DateTime('2018-05-04 12:31:00'));
        $this->assertEquals('2018-05-04 17:00:00', $next->format('Y-m-d H:i:s'));

        $next = $this->shop->nextClosed(new DateTime('2018-05-04 17:30:00'));
        $this->assertEquals('2018-05-07 12:20:00', $next->format('Y-m-d H:i:s'));

        $next = $this->shop->nextClosed(new DateTime('2017-01-01 08:55:55'));
        $this->assertEquals('2017-01-02 12:20:00', $next->format('Y-m-d H:i:s'));
    }
}
